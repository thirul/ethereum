package com.boomi.connector.ethereum.decode;

import org.json.JSONArray;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Array;
import org.web3j.abi.datatypes.Bytes;
import org.web3j.abi.datatypes.DynamicBytes;
import org.web3j.abi.datatypes.Type;
import org.web3j.utils.Numeric;

public class FunctionReturnType {
	public static final String STATIC_ARRAY = "staticarray";
	public static final String DYNAMIC_ARRAY = "dynamicarray";
	private final String _type;
	private final int _arraySize;
	private final Class<Type> _arrayParamiterizedClass;
	private final TypeReference<?> _arrayTypeReference;
	
	private FunctionReturnType(String type, int size, Class<Type> arrayParamiterizedClass, TypeReference<?> arrayTypeReference) {
		_type = type;
		_arraySize = size;
		_arrayParamiterizedClass = arrayParamiterizedClass;
		_arrayTypeReference = arrayTypeReference;
	}

	public FunctionReturnType(String type) {
		this(type, 0, null, null);
	}
	
	public FunctionReturnType(int size, Class<Type> arrayParamiterizedClass, TypeReference<?> arrayTypeReference) {
		this(STATIC_ARRAY, size, arrayParamiterizedClass, arrayTypeReference);
	}
	
	public FunctionReturnType(Class<Type> arrayParamiterizedClass, TypeReference<?> arrayTypeReference) {
		this(DYNAMIC_ARRAY, 0, arrayParamiterizedClass, arrayTypeReference);
	}
	
	public String getType() {
		return _type;
	}

	public int getArraySize() {
		return _arraySize;
	}

	public Class<Type> getArrayParamiterizedClass() {
		return _arrayParamiterizedClass;
	}

	public TypeReference<?> getArrayTypeReference() {
		return _arrayTypeReference;
	}
	
	public void convert(Type value, JSONArray jsonArray) {
		if (_type.equals(STATIC_ARRAY) || _type.equals(DYNAMIC_ARRAY)) {
			Array<Type> array = (Array<Type>)value;
			JSONArray ja = new JSONArray();
			for (Type t: array.getValue()) {
				ja.put(t.getValue());
			}
			jsonArray.put(ja);
		} else if (value.getTypeAsString().equals(Bytes.TYPE_NAME)) {
			/*
			Bytes bytes = (Bytes)value;
			byte[] bytesValue = bytes.getValue();
			JSONArray ja = new JSONArray();
			for (byte b: bytesValue) {
				ja.put(Byte.toString(b));
			}
			jsonArray.put(ja);
			*/
			DynamicBytes bytes = (DynamicBytes)value;
			jsonArray.put(Numeric.toHexString(bytes.getValue()));
		} else if (value.getTypeAsString().startsWith(Bytes.TYPE_NAME)) {
			Bytes bytes = (Bytes)value;
			jsonArray.put(Numeric.toHexString(bytes.getValue()));
		} else {
			jsonArray.put(value.getValue());
		}
	}
}
