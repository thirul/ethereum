package com.boomi.connector.ethereum.decode;
/*
 * The methods in this class are basically copies of ones found in org.web3j.abi.TypeDecoder but with an additional parameter
 * identifying the Parameterized class type. This is required as it is not possible to determine Parameterized classes via
 * reflection.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Array;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.StaticArray;
import org.web3j.abi.datatypes.Type;

public class ArrayTypeDecoder {
	
    public static <T extends Type> int getDataOffset(String input, int offset, FunctionReturnType returnType) {
        if (returnType.getType().equals("bytes")
                || returnType.getType().equals("string")
                || returnType.getType().equals("dynamicarray")) {
            return TypeDecoder.decodeUintAsInt(input, offset) << 1;
        } else {
            return offset;
        }
    }
    
    public static <T extends Type> T decodeStaticArray(String input, int offset, TypeReference<T> typeReference, Class<T> cls, int length) {

        BiFunction<List<T>, String, T> function = (elements, typeName) -> {
            if (elements.isEmpty()) {
                throw new UnsupportedOperationException("Zero length fixed array is invalid type");
            } else {
                return instantiateStaticArray(typeReference, elements);
            }
        };

        return decodeArrayElements(input, offset, typeReference, cls, length, function);
    }

    @SuppressWarnings("unchecked")
    private static <T extends Type> T instantiateStaticArray(TypeReference<T> typeReference, List<T> elements) {
        try {
            Class<List> listClass = List.class;
            return typeReference.getClassType().getConstructor(listClass).newInstance(elements);
        } catch (ReflectiveOperationException e) {
            //noinspection unchecked
            return (T) new StaticArray<>(elements);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends Type> T decodeDynamicArray(String input, int offset, TypeReference<T> typeReference, Class<T> cls) {

        int length = TypeDecoder.decodeUintAsInt(input, offset);

        BiFunction<List<T>, String, T> function = (elements, typeName) -> {
            if (elements.isEmpty()) {
                return (T) DynamicArray.empty(typeName);
            } else {
                return (T) new DynamicArray<>(elements);
            }
        };

        int valueOffset = offset + TypeDecoder.MAX_BYTE_LENGTH_FOR_HEX_STRING;

        return decodeArrayElements(input, valueOffset, typeReference, cls, length, function);
    }

    private static <T extends Type> T decodeArrayElements(String input, int offset, TypeReference<T> typeReference, Class<T> cls, int length, BiFunction<List<T>, String, T> consumer) {

        if (Array.class.isAssignableFrom(cls)) {
            throw new UnsupportedOperationException(
                    "Arrays of arrays are not currently supported for external functions, see"
                            + "http://solidity.readthedocs.io/en/develop/types.html#members");
        } else {
            List<T> elements = new ArrayList<>(length);

            for (int i = 0, currOffset = offset;
                    i < length;
                    i++, currOffset += TypeDecoder.getSingleElementLength(input, currOffset, cls)
                         * TypeDecoder.MAX_BYTE_LENGTH_FOR_HEX_STRING) {
            	
                T value = TypeDecoder.decode(input, currOffset, cls);
                elements.add(value);
            }

            String typeName = Utils.getSimpleTypeName(cls);

            return consumer.apply(elements, typeName);
        }
    }
}
