package com.boomi.connector.ethereum.util;

import java.io.IOException;

import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.utils.Numeric;

public class PrivateKeyExtractor {

	public static void main(String[] args) {
		if (args.length > 1) {
			new PrivateKeyExtractor(args[0], args[1]);
		} else {
			System.out.println("usage jar -jar PrivateKeyExtractor.jar [wallet file path] [wallet file password]");
		}
	}

	public PrivateKeyExtractor(String walletFilePath, String password) {
		try {
			Credentials creds = WalletUtils.loadCredentials(password, walletFilePath);
			System.out.println(Numeric.toHexStringWithPrefix(creds.getEcKeyPair().getPrivateKey()));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (CipherException e) {
			e.printStackTrace();
		}
	}
}
