package com.boomi.connector.ethereum;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import org.json.JSONException;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.util.BaseBrowser;
import com.boomi.util.StringUtil;

public class EthereumBrowser extends BaseBrowser {
	private static final String ARGS_INVOKE = "EthereumInvokeArgs";

	protected EthereumBrowser(EthereumConnection conn) {
		super(conn);
	}

	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId, Collection<ObjectDefinitionRole> roles) {
	    ObjectDefinitions defs = new ObjectDefinitions();
		try {
	        ObjectDefinition def = new ObjectDefinition();
	        def.setInputType(ContentType.JSON);
	        def.setOutputType(ContentType.JSON);
	        def.setElementName(StringUtil.EMPTY_STRING);
	        if (objectTypeId.equals(ARGS_INVOKE)) {
	        	def.setJsonSchema(readJsonSchema("ethereum_args_schema.json"));
	        }
		    defs.getDefinitions().add(def);
		} catch (IOException e) {
			throw new ConnectorException(e);
		}
	    
		return defs;
	}

	@Override
	public ObjectTypes getObjectTypes() {
	    ObjectTypes objectTypes = new ObjectTypes();
        ObjectType deployOT = new ObjectType();
        deployOT.setId(ARGS_INVOKE);
        deployOT.setLabel("Ethereum Invoke Arguments");
        objectTypes.getTypes().add(deployOT);
	    
		return objectTypes;
	}
	
	private static String readJsonSchema(String path) throws JSONException, IOException {
		InputStream is = null;
	    try {
	    	is = EthereumBrowser.class.getResourceAsStream("/"+path);
            return toString(is, java.nio.charset.StandardCharsets.UTF_8.toString());
        } finally {
        	if (is != null) {
        		is.close();
        	}
        }
	}
	
    public static String toString(InputStream in, String charsetName) throws IOException {
    	ByteArrayOutputStream bout = new ByteArrayOutputStream();
        byte[] buf = new byte[8192];
        for( int len; ( len = in.read(buf) ) != -1; ) {
        	bout.write(buf, 0, len);
        }
        return bout.toString(charsetName);
    }
}
