package com.boomi.connector.ethereum;

public enum EthereumOperationType {
	INVOKE_CONSTANT,
	INVOKE_NONCONSTANT
}
